"use strict";
(() => {
var exports = {};
exports.id = 7605;
exports.ids = [7605];
exports.modules = {

/***/ 2616:
/***/ ((module) => {

module.exports = require("formidable");

/***/ }),

/***/ 1185:
/***/ ((module) => {

module.exports = require("mongoose");

/***/ }),

/***/ 4023:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "config": () => (/* binding */ config),
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _tools_FormDataParser__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(2825);
/* harmony import */ var _tools_FormDataParser__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_tools_FormDataParser__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _tools_db_Model_ContactModel__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(9187);
/* harmony import */ var _tools_db_Model_ContactModel__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_tools_db_Model_ContactModel__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _tools_db_connection__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(6704);
/* harmony import */ var _tools_db_connection__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_tools_db_connection__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _tools_Response__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(3829);
/* harmony import */ var _tools_Response__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_tools_Response__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _tools_validators_contact_createContactValidator__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(3109);
/* harmony import */ var _tools_validators_contact_createContactValidator__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_tools_validators_contact_createContactValidator__WEBPACK_IMPORTED_MODULE_4__);




// import validators

const config = {
    api: {
        bodyParser: false
    }
};
/**
 * @method POST
 *
 * @requires FromData
 *
 * @param name String
 * @param subject String [optional]
 * @param phone String
 * @param email String
 * @param message String
 *
 * @return ContactModel
 *
 * */ /* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (async (req, res)=>{
    const { method  } = req;
    if (method != 'POST') {
        return res.status(404).send();
    }
    try {
        var ref, ref1, ref2, ref3, ref4;
        const { files , fields  } = await _tools_FormDataParser__WEBPACK_IMPORTED_MODULE_0___default()(req);
        const errors = _tools_validators_contact_createContactValidator__WEBPACK_IMPORTED_MODULE_4___default()({
            files,
            fields
        });
        if (errors) {
            return res.status(422).send(_tools_Response__WEBPACK_IMPORTED_MODULE_3___default()({
                status_code: 422,
                errors,
                message: 'Invalid input!'
            }));
        }
        await _tools_db_connection__WEBPACK_IMPORTED_MODULE_2___default()();
        // save data to db
        const contact = await _tools_db_Model_ContactModel__WEBPACK_IMPORTED_MODULE_1___default().create({
            name: (fields === null || fields === void 0 ? void 0 : (ref = fields.name) === null || ref === void 0 ? void 0 : ref.trim()) || '',
            subject: (fields === null || fields === void 0 ? void 0 : (ref1 = fields.subject) === null || ref1 === void 0 ? void 0 : ref1.trim()) || '',
            phone: (fields === null || fields === void 0 ? void 0 : (ref2 = fields.phone) === null || ref2 === void 0 ? void 0 : ref2.trim()) || '',
            email: (fields === null || fields === void 0 ? void 0 : (ref3 = fields.email) === null || ref3 === void 0 ? void 0 : ref3.trim()) || '',
            message: (fields === null || fields === void 0 ? void 0 : (ref4 = fields.message) === null || ref4 === void 0 ? void 0 : ref4.trim()) || ''
        });
        // return to client
        return res.status(201).send(_tools_Response__WEBPACK_IMPORTED_MODULE_3___default()({
            data: contact,
            message: 'Message Sent Successful!',
            status_code: 201
        }));
    } catch (e) {
        res.status(500).send(_tools_Response__WEBPACK_IMPORTED_MODULE_3___default()({
            message: 'Message Sent Failed!',
            status_code: 500
        }));
    }
});


/***/ }),

/***/ 9187:
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {


const mongoose = __webpack_require__(1185);
const contactSchema = new mongoose.Schema({
    name: {
        required: true,
        type: String
    },
    email: {
        required: true,
        type: String
    },
    subject: {
        required: false,
        type: String
    },
    phone: {
        required: false,
        type: String
    },
    message: {
        required: true,
        type: mongoose.Schema.Types.String
    }
}, {
    timestamps: {
        createdAt: 'created_at',
        updatedAt: 'updated_at'
    }
});
const ContactModel = mongoose.models.Contact || mongoose.model('Contact', contactSchema);
module.exports = ContactModel;


/***/ }),

/***/ 3109:
/***/ ((module) => {


const createContactValidator = ({ files , fields  })=>{
    const errors = {
    };
    // name validator
    if (fields === null || fields === void 0 ? void 0 : fields.name) {
    } else {
        errors.name = 'Name is required!';
    }
    // Email validator
    if (fields === null || fields === void 0 ? void 0 : fields.email) {
    } else {
        errors.email = 'Email is required!';
    }
    // Phone validator
    if (fields === null || fields === void 0 ? void 0 : fields.phone) {
    } else {
        errors.phone = 'Phone is required!';
    }
    // Phone validator
    if (fields === null || fields === void 0 ? void 0 : fields.message) {
    } else {
        errors.message = 'Message is required!';
    }
    if (Object.keys(errors).length) {
        return errors;
    }
    return null;
};
module.exports = createContactValidator;


/***/ })

};
;

// load runtime
var __webpack_require__ = require("../../../webpack-api-runtime.js");
__webpack_require__.C(exports);
var __webpack_exec__ = (moduleId) => (__webpack_require__(__webpack_require__.s = moduleId))
var __webpack_exports__ = __webpack_require__.X(0, [6676], () => (__webpack_exec__(4023)));
module.exports = __webpack_exports__;

})();