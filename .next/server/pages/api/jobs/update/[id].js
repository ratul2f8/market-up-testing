"use strict";
(() => {
var exports = {};
exports.id = 2591;
exports.ids = [2591];
exports.modules = {

/***/ 2616:
/***/ ((module) => {

module.exports = require("formidable");

/***/ }),

/***/ 1185:
/***/ ((module) => {

module.exports = require("mongoose");

/***/ }),

/***/ 2423:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "config": () => (/* binding */ config),
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _tools_db_connection__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(6704);
/* harmony import */ var _tools_db_connection__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_tools_db_connection__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _tools_db_Model_JobsModel__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(5388);
/* harmony import */ var _tools_db_Model_JobsModel__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_tools_db_Model_JobsModel__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var mongoose__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(1185);
/* harmony import */ var mongoose__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(mongoose__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _tools_FormDataParser__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(2825);
/* harmony import */ var _tools_FormDataParser__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_tools_FormDataParser__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _tools_validators_jobs_jobsDataValidator__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(5770);
/* harmony import */ var _tools_validators_jobs_jobsDataValidator__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_tools_validators_jobs_jobsDataValidator__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _tools_Response__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(3829);
/* harmony import */ var _tools_Response__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(_tools_Response__WEBPACK_IMPORTED_MODULE_5__);






const config = {
    api: {
        bodyParser: false
    }
};
/**
 * @method PUT
 *
 * @requires FromData
 * 
 * @param position String
 * @param vacancy Number
 * @param job_type String
 * @param last_date Date
 * @param fb String
 * @param insta String
 *
 * @return JobsModel
 *
 * */ const handler = async (req, res)=>{
    if (req.method !== 'PUT') {
        return res.status(404).send();
    }
    try {
        var ref, ref1, ref2, ref3;
        const { query: { id: jobsId  }  } = req;
        // object id validation
        if (!mongoose__WEBPACK_IMPORTED_MODULE_2___default().Types.ObjectId.isValid(jobsId)) {
            return res.status(404).send();
        }
        await _tools_db_connection__WEBPACK_IMPORTED_MODULE_0___default()();
        const jobs = await _tools_db_Model_JobsModel__WEBPACK_IMPORTED_MODULE_1___default().findById(jobsId);
        // db existence validation
        if (!jobs) {
            return res.status(404).send();
        }
        // parse the form from FormData
        const { fields  } = await _tools_FormDataParser__WEBPACK_IMPORTED_MODULE_3___default()(req);
        const errors = _tools_validators_jobs_jobsDataValidator__WEBPACK_IMPORTED_MODULE_4___default()({
            fields
        });
        // return if error
        if (errors) {
            return res.status(422).send(_tools_Response__WEBPACK_IMPORTED_MODULE_5___default()({
                errors,
                status_code: 422,
                message: 'Invalid inputs!'
            }));
        }
        //alter data if any updated
        jobs.position = fields === null || fields === void 0 ? void 0 : (ref = fields.position) === null || ref === void 0 ? void 0 : ref.trim(), jobs.vacancy = fields === null || fields === void 0 ? void 0 : fields.vacancy, jobs.jobType = fields === null || fields === void 0 ? void 0 : (ref1 = fields.jobType) === null || ref1 === void 0 ? void 0 : ref1.trim(), jobs.lastDate = fields === null || fields === void 0 ? void 0 : fields.lastDate, jobs.fb = fields === null || fields === void 0 ? void 0 : (ref2 = fields.fb) === null || ref2 === void 0 ? void 0 : ref2.trim(), jobs.insta = fields === null || fields === void 0 ? void 0 : (ref3 = fields.insta) === null || ref3 === void 0 ? void 0 : ref3.trim();
        await jobs.save();
        return res.status(201).send(_tools_Response__WEBPACK_IMPORTED_MODULE_5___default()({
            data: jobs,
            status_code: 201,
            message: 'Update complete!'
        }));
    } catch (e) {
        res.status(500).send(_tools_Response__WEBPACK_IMPORTED_MODULE_5___default()({
            message: e.message,
            status_code: 500
        }));
    }
};
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (handler);


/***/ })

};
;

// load runtime
var __webpack_require__ = require("../../../../webpack-api-runtime.js");
__webpack_require__.C(exports);
var __webpack_exec__ = (moduleId) => (__webpack_require__(__webpack_require__.s = moduleId))
var __webpack_exports__ = __webpack_require__.X(0, [6676,6177], () => (__webpack_exec__(2423)));
module.exports = __webpack_exports__;

})();