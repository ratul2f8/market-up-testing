"use strict";
(() => {
var exports = {};
exports.id = 9712;
exports.ids = [9712];
exports.modules = {

/***/ 2616:
/***/ ((module) => {

module.exports = require("formidable");

/***/ }),

/***/ 1185:
/***/ ((module) => {

module.exports = require("mongoose");

/***/ }),

/***/ 6934:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "config": () => (/* binding */ config),
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _tools_FormDataParser__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(2825);
/* harmony import */ var _tools_FormDataParser__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_tools_FormDataParser__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _tools_db_Model_JobsModel__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(5388);
/* harmony import */ var _tools_db_Model_JobsModel__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_tools_db_Model_JobsModel__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _tools_db_connection__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(6704);
/* harmony import */ var _tools_db_connection__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_tools_db_connection__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _tools_Response__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(3829);
/* harmony import */ var _tools_Response__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_tools_Response__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _tools_validators_jobs_jobsDataValidator__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(5770);
/* harmony import */ var _tools_validators_jobs_jobsDataValidator__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_tools_validators_jobs_jobsDataValidator__WEBPACK_IMPORTED_MODULE_4__);




// import validators

const config = {
    api: {
        bodyParser: false
    }
};
/**
 * @method POST
 *
 * @requires FromData
 *
 * @param position String
 * @param vacancy Number
 * @param jobType String
 * @param lastDate Date
 * @param fb String
 * @param insta String
 *
 * @return JobsModel
 *
 * */ /* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (async (req, res)=>{
    const { method  } = req;
    if (method != 'POST') {
        return res.status(404).send();
    }
    try {
        var ref, ref1, ref2, ref3;
        await _tools_db_connection__WEBPACK_IMPORTED_MODULE_2___default()();
        const { fields  } = await _tools_FormDataParser__WEBPACK_IMPORTED_MODULE_0___default()(req);
        const errors = _tools_validators_jobs_jobsDataValidator__WEBPACK_IMPORTED_MODULE_4___default()({
            fields
        });
        if (errors) {
            return res.status(422).send(_tools_Response__WEBPACK_IMPORTED_MODULE_3___default()({
                status_code: 422,
                errors,
                message: 'Invalid inputs!'
            }));
        }
        // save data to db
        const jobs = await _tools_db_Model_JobsModel__WEBPACK_IMPORTED_MODULE_1___default().create({
            position: fields === null || fields === void 0 ? void 0 : (ref = fields.position) === null || ref === void 0 ? void 0 : ref.trim(),
            vacancy: fields === null || fields === void 0 ? void 0 : fields.vacancy,
            jobType: fields === null || fields === void 0 ? void 0 : (ref1 = fields.jobType) === null || ref1 === void 0 ? void 0 : ref1.trim(),
            lastDate: fields === null || fields === void 0 ? void 0 : fields.lastDate,
            fb: fields === null || fields === void 0 ? void 0 : (ref2 = fields.fb) === null || ref2 === void 0 ? void 0 : ref2.trim(),
            insta: fields === null || fields === void 0 ? void 0 : (ref3 = fields.insta) === null || ref3 === void 0 ? void 0 : ref3.trim()
        });
        // return to client
        return res.status(201).send(_tools_Response__WEBPACK_IMPORTED_MODULE_3___default()({
            data: jobs,
            message: 'New jobs added!',
            status_code: 201
        }));
    } catch (e) {
        res.status(400).send(_tools_Response__WEBPACK_IMPORTED_MODULE_3___default()({
            message: e.message,
            status_code: 400
        }));
    }
});


/***/ })

};
;

// load runtime
var __webpack_require__ = require("../../../webpack-api-runtime.js");
__webpack_require__.C(exports);
var __webpack_exec__ = (moduleId) => (__webpack_require__(__webpack_require__.s = moduleId))
var __webpack_exports__ = __webpack_require__.X(0, [6676,6177], () => (__webpack_exec__(6934)));
module.exports = __webpack_exports__;

})();