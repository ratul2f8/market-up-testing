"use strict";
(() => {
var exports = {};
exports.id = 9269;
exports.ids = [9269];
exports.modules = {

/***/ 3518:
/***/ ((module) => {

module.exports = require("cloudinary");

/***/ }),

/***/ 384:
/***/ ((module) => {

module.exports = require("datauri");

/***/ }),

/***/ 2616:
/***/ ((module) => {

module.exports = require("formidable");

/***/ }),

/***/ 1185:
/***/ ((module) => {

module.exports = require("mongoose");

/***/ }),

/***/ 8519:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "config": () => (/* binding */ config),
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _tools_FormDataParser__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(2825);
/* harmony import */ var _tools_FormDataParser__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_tools_FormDataParser__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _tools_cloudinary_upload__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(9053);
/* harmony import */ var _tools_cloudinary_upload__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_tools_cloudinary_upload__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _tools_db_Model_PartnershipModel__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(9393);
/* harmony import */ var _tools_db_Model_PartnershipModel__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_tools_db_Model_PartnershipModel__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _tools_db_connection__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(6704);
/* harmony import */ var _tools_db_connection__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_tools_db_connection__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _tools_Response__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(3829);
/* harmony import */ var _tools_Response__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_tools_Response__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _tools_validators_partner_createPartnerValidator__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(3355);
/* harmony import */ var _tools_validators_partner_createPartnerValidator__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(_tools_validators_partner_createPartnerValidator__WEBPACK_IMPORTED_MODULE_5__);





// import validators

const config = {
    api: {
        bodyParser: false
    }
};
/**
 * @method POST
 *
 * @requires FromData
 *
 * @param name String
 * @param designation String
 * @param text String
 * @param image File image/png, image/jpg, image/jpeg
 * @param company_logo File image/png, image/jpg, image/jpeg
 *
 * @return [PartnershipModel]
 *
 * */ /* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (async (req, res)=>{
    const { method  } = req;
    if (method != 'POST') {
        return res.status(404).send();
    }
    try {
        await _tools_db_connection__WEBPACK_IMPORTED_MODULE_3___default()();
        const { files , fields  } = await _tools_FormDataParser__WEBPACK_IMPORTED_MODULE_0___default()(req);
        const errors = _tools_validators_partner_createPartnerValidator__WEBPACK_IMPORTED_MODULE_5___default()({
            files,
            fields
        });
        if (errors) {
            return res.status(422).send(_tools_Response__WEBPACK_IMPORTED_MODULE_4___default()({
                status_code: 422,
                errors,
                message: 'Invalid input!'
            }));
        }
        const uploadedImage = await _tools_cloudinary_upload__WEBPACK_IMPORTED_MODULE_1___default()(files.image, 'partner');
        const companyLogo = await _tools_cloudinary_upload__WEBPACK_IMPORTED_MODULE_1___default()(files.company_logo, 'partner');
        // save data to db
        const partner = await _tools_db_Model_PartnershipModel__WEBPACK_IMPORTED_MODULE_2___default().create({
            name: fields.name.trim(),
            designation: fields.designation.trim(),
            image: uploadedImage.public_id,
            company_logo: companyLogo.public_id,
            text: fields.text.trim()
        });
        // return to client
        return res.status(201).send(_tools_Response__WEBPACK_IMPORTED_MODULE_4___default()({
            data: partner,
            message: 'New partner added!',
            status_code: 201
        }));
    } catch (e) {
        res.status(500).send(_tools_Response__WEBPACK_IMPORTED_MODULE_4___default()({
            message: e.message,
            status_code: 500
        }));
    }
});


/***/ }),

/***/ 381:
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {


const cloudinary = __webpack_require__(3518);
cloudinary.config({
    cloud_name: 'dhrjql4ev',
    api_key: '346684836276456',
    api_secret: 'r5GE_y1LszfXliaz3GY6qyDc6Vk'
});
module.exports = cloudinary;


/***/ }),

/***/ 9053:
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {


const cloudinary = __webpack_require__(381);
const datauri = __webpack_require__(384);
/**
 *
 * @param file File
 * @param folder String
 *
 * @return {public_id, ...}
 *
 * */ module.exports = (file, folder)=>{
    return new Promise((resolve, reject)=>{
        datauri(file.path).then((bs)=>{
            cloudinary.v2.uploader.upload(bs, {
                folder: folder
            }, (err, result)=>{
                if (err) {
                    reject(err);
                }
                resolve(result);
            });
        }).catch((err)=>{
            reject(err);
        });
    });
};


/***/ }),

/***/ 9393:
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {


var ref;
const mongoose = __webpack_require__(1185);
const partnerSchema = new mongoose.Schema({
    name: String,
    designation: String,
    image: String,
    company_logo: String,
    text: String
}, {
    timestamps: {
        createdAt: 'created_at',
        updatedAt: 'updated_at'
    }
});
const PartnershipModel = ((ref = mongoose.models) === null || ref === void 0 ? void 0 : ref.Partnership) || mongoose.model('Partnership', partnerSchema);
module.exports = PartnershipModel;


/***/ }),

/***/ 3355:
/***/ ((module) => {


const createPartnerValidator = ({ files , fields  })=>{
    const errors = {
    };
    // image validator
    if (files === null || files === void 0 ? void 0 : files.image) {
        if (files.image.type == 'image/png' || files.image.type == 'image/jpg' || files.image.type == 'image/jpeg') {
        } else {
            errors.image = 'Only \'jpg, png, jpeg\' are allowed!';
        }
    } else {
        errors.image = 'Image is required!';
    }
    // company logo validator
    if (files === null || files === void 0 ? void 0 : files.company_logo) {
        if (files.company_logo.type == 'image/png' || files.company_logo.type == 'image/jpg' || files.company_logo.type == 'image/jpeg') {
        } else {
            errors.company_logo = 'Only \'jpg, png, jpeg\' are allowed!';
        }
    } else {
        errors.company_logo = 'Image is required!';
    }
    // name validator
    if (fields === null || fields === void 0 ? void 0 : fields.name) {
        if (fields.name.trim() > 12) {
            errors.name = 'Maximum 12 character allow!';
        } else if (fields.name.trim() < 3) {
            errors.name = 'Minimum 3 character allow!';
        }
    } else {
        errors.name = 'Name is required!';
    }
    // designation validator
    if (fields === null || fields === void 0 ? void 0 : fields.designation) {
        if (fields.designation.trim() > 15) {
            errors.designation = 'Maximum 15 character allow!';
        } else if (fields.designation.trim() < 3) {
            errors.designation = 'Minimum 3 character allow!';
        }
    } else {
        errors.designation = 'Designation is required!';
    }
    // text validator
    if (fields === null || fields === void 0 ? void 0 : fields.text) {
        if (fields.text.trim() > 500) {
            errors.text = 'Maximum 500 character allow!';
        } else if (fields.text.trim() < 150) {
            errors.text = 'Minimum 150 character allow!';
        }
    } else {
        errors.text = 'Text is required!';
    }
    if (Object.keys(errors).length) {
        return errors;
    }
    return null;
};
module.exports = createPartnerValidator;


/***/ })

};
;

// load runtime
var __webpack_require__ = require("../../../webpack-api-runtime.js");
__webpack_require__.C(exports);
var __webpack_exec__ = (moduleId) => (__webpack_require__(__webpack_require__.s = moduleId))
var __webpack_exports__ = __webpack_require__.X(0, [6676], () => (__webpack_exec__(8519)));
module.exports = __webpack_exports__;

})();