"use strict";
(() => {
var exports = {};
exports.id = 8353;
exports.ids = [8353];
exports.modules = {

/***/ 3518:
/***/ ((module) => {

module.exports = require("cloudinary");

/***/ }),

/***/ 384:
/***/ ((module) => {

module.exports = require("datauri");

/***/ }),

/***/ 2616:
/***/ ((module) => {

module.exports = require("formidable");

/***/ }),

/***/ 1185:
/***/ ((module) => {

module.exports = require("mongoose");

/***/ }),

/***/ 5742:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "config": () => (/* binding */ config),
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _tools_db_connection__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(6704);
/* harmony import */ var _tools_db_connection__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_tools_db_connection__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _tools_db_Model_TeamModel__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(6196);
/* harmony import */ var _tools_db_Model_TeamModel__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_tools_db_Model_TeamModel__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var mongoose__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(1185);
/* harmony import */ var mongoose__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(mongoose__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _tools_FormDataParser__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(2825);
/* harmony import */ var _tools_FormDataParser__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_tools_FormDataParser__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _tools_cloudinary_upload__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(9053);
/* harmony import */ var _tools_cloudinary_upload__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_tools_cloudinary_upload__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _tools_validators_team_updateTeamValidator__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(3379);
/* harmony import */ var _tools_validators_team_updateTeamValidator__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(_tools_validators_team_updateTeamValidator__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _tools_Response__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(3829);
/* harmony import */ var _tools_Response__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(_tools_Response__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var _tools_cloudinary_delete__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(3834);
/* harmony import */ var _tools_cloudinary_delete__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(_tools_cloudinary_delete__WEBPACK_IMPORTED_MODULE_7__);








const config = {
    api: {
        bodyParser: false
    }
};
/**
 * @method PUT
 *
 * @requires FromData
 *
 * @param name String
 * @param designation String
 * @param image File image/png, image/jpg, image/jpeg [optional]
 *
 * @return TeamModel
 *
 * */ const handler = async (req, res)=>{
    if (req.method !== 'PUT') {
        return res.status(404).send();
    }
    try {
        const { query: { id: teamId  }  } = req;
        // object id validation
        if (!mongoose__WEBPACK_IMPORTED_MODULE_2___default().Types.ObjectId.isValid(teamId)) {
            return res.status(404).send();
        }
        await _tools_db_connection__WEBPACK_IMPORTED_MODULE_0___default()();
        const team = await _tools_db_Model_TeamModel__WEBPACK_IMPORTED_MODULE_1___default().findById(teamId);
        // db existence validation
        if (!team) {
            return res.status(404).send();
        }
        // parse the form from FormData
        const { files , fields  } = await _tools_FormDataParser__WEBPACK_IMPORTED_MODULE_3___default()(req);
        const errors = _tools_validators_team_updateTeamValidator__WEBPACK_IMPORTED_MODULE_5___default()({
            files,
            fields
        });
        // return if error
        if (errors) {
            return res.status(422).send(_tools_Response__WEBPACK_IMPORTED_MODULE_6___default()({
                errors,
                status_code: 422,
                message: 'Invalid input!'
            }));
        }
        team.name = fields.name.trim();
        team.designation = fields.designation.trim();
        const prevId = team.image;
        if (files === null || files === void 0 ? void 0 : files.image) {
            const { public_id  } = await _tools_cloudinary_upload__WEBPACK_IMPORTED_MODULE_4___default()(files.image, 'team');
            team.image = public_id;
        }
        await team.save();
        // delete previous image if new have
        if (files === null || files === void 0 ? void 0 : files.image) {
            await _tools_cloudinary_delete__WEBPACK_IMPORTED_MODULE_7___default()(prevId);
        }
        return res.status(201).send(_tools_Response__WEBPACK_IMPORTED_MODULE_6___default()({
            data: team,
            status_code: 201,
            message: 'Updated complete!'
        }));
    } catch (e) {
        res.status(500).send(_tools_Response__WEBPACK_IMPORTED_MODULE_6___default()({
            message: e.message,
            status_code: 500
        }));
    }
};
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (handler);


/***/ }),

/***/ 3834:
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {


const cloudinary = __webpack_require__(381);
const deleteImage = (publicId)=>cloudinary.v2.uploader.destroy(publicId)
;
module.exports = deleteImage;


/***/ }),

/***/ 381:
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {


const cloudinary = __webpack_require__(3518);
cloudinary.config({
    cloud_name: 'dhrjql4ev',
    api_key: '346684836276456',
    api_secret: 'r5GE_y1LszfXliaz3GY6qyDc6Vk'
});
module.exports = cloudinary;


/***/ }),

/***/ 9053:
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {


const cloudinary = __webpack_require__(381);
const datauri = __webpack_require__(384);
/**
 *
 * @param file File
 * @param folder String
 *
 * @return {public_id, ...}
 *
 * */ module.exports = (file, folder)=>{
    return new Promise((resolve, reject)=>{
        datauri(file.path).then((bs)=>{
            cloudinary.v2.uploader.upload(bs, {
                folder: folder
            }, (err, result)=>{
                if (err) {
                    reject(err);
                }
                resolve(result);
            });
        }).catch((err)=>{
            reject(err);
        });
    });
};


/***/ }),

/***/ 6196:
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {


const mongoose = __webpack_require__(1185);
const team = new mongoose.Schema({
    name: {
        required: true,
        type: String
    },
    designation: {
        required: true,
        type: String
    },
    image: {
        required: true,
        type: String
    }
}, {
    timestamps: {
        createdAt: 'created_at',
        updatedAt: 'updated_at'
    }
});
const User = mongoose.models.Team || mongoose.model('Team', team);
module.exports = User;


/***/ }),

/***/ 3379:
/***/ ((module) => {


const updateTeamValidator = ({ files , fields  })=>{
    const errors = {
    };
    // name validator
    if (fields === null || fields === void 0 ? void 0 : fields.name) {
        if (fields.name.trim().length < 3) {
            errors.name = 'Minimum 3 character allows!';
        }
    } else {
        errors.name = 'Name is required!';
    }
    // designation validator
    if (fields === null || fields === void 0 ? void 0 : fields.designation) {
        if (fields.designation.trim().length < 2) {
            errors.designation = 'Minimum 2 character allows!';
        }
    } else {
        errors.designation = 'Designation is required!';
    }
    // image validator
    if (files === null || files === void 0 ? void 0 : files.image) {
        if (files.image.type == 'image/png' || files.image.type == 'image/jpg' || files.image.type == 'image/jpeg') {
        } else {
            errors.image = 'Only \'jpg, png, jpeg\' are allowed!';
        }
    }
    if (Object.keys(errors).length) {
        return errors;
    }
    return null;
};
module.exports = updateTeamValidator;


/***/ })

};
;

// load runtime
var __webpack_require__ = require("../../../../webpack-api-runtime.js");
__webpack_require__.C(exports);
var __webpack_exec__ = (moduleId) => (__webpack_require__(__webpack_require__.s = moduleId))
var __webpack_exports__ = __webpack_require__.X(0, [6676], () => (__webpack_exec__(5742)));
module.exports = __webpack_exports__;

})();