"use strict";
(() => {
var exports = {};
exports.id = 4209;
exports.ids = [4209];
exports.modules = {

/***/ 3518:
/***/ ((module) => {

module.exports = require("cloudinary");

/***/ }),

/***/ 384:
/***/ ((module) => {

module.exports = require("datauri");

/***/ }),

/***/ 2616:
/***/ ((module) => {

module.exports = require("formidable");

/***/ }),

/***/ 1185:
/***/ ((module) => {

module.exports = require("mongoose");

/***/ }),

/***/ 7374:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "config": () => (/* binding */ config),
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _tools_FormDataParser__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(2825);
/* harmony import */ var _tools_FormDataParser__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_tools_FormDataParser__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _tools_cloudinary_upload__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(9053);
/* harmony import */ var _tools_cloudinary_upload__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_tools_cloudinary_upload__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _tools_db_Model_ClientsModel__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(6775);
/* harmony import */ var _tools_db_Model_ClientsModel__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_tools_db_Model_ClientsModel__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _tools_db_connection__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(6704);
/* harmony import */ var _tools_db_connection__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_tools_db_connection__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _tools_Response__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(3829);
/* harmony import */ var _tools_Response__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_tools_Response__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _tools_validators_clients_clientDataValidator__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(7900);
/* harmony import */ var _tools_validators_clients_clientDataValidator__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(_tools_validators_clients_clientDataValidator__WEBPACK_IMPORTED_MODULE_5__);





// import validators

const config = {
    api: {
        bodyParser: false
    }
};
/**
 * @method POST
 *
 * @requires FromData

 * @param image File image/png, image/jpg, image/jpeg
 *
 * @return ClientModel
 *
 * */ /* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (async (req, res)=>{
    const { method  } = req;
    if (method != 'POST') {
        return res.send(method + " method is not allowed!");
    }
    try {
        await _tools_db_connection__WEBPACK_IMPORTED_MODULE_3___default()();
        const { files  } = await _tools_FormDataParser__WEBPACK_IMPORTED_MODULE_0___default()(req);
        // const create = true;
        const errors = _tools_validators_clients_clientDataValidator__WEBPACK_IMPORTED_MODULE_5___default()({
            files,
            create: true
        });
        if (errors) {
            return res.status(422).send(_tools_Response__WEBPACK_IMPORTED_MODULE_4___default()({
                status_code: 422,
                errors,
                message: 'Invalid input!'
            }));
        }
        const { public_id  } = await _tools_cloudinary_upload__WEBPACK_IMPORTED_MODULE_1___default()(files.image, 'clients');
        // save data to db
        const clients = await _tools_db_Model_ClientsModel__WEBPACK_IMPORTED_MODULE_2___default().create({
            image: public_id
        });
        // return to client
        return res.status(201).send(_tools_Response__WEBPACK_IMPORTED_MODULE_4___default()({
            data: clients,
            message: 'New client logo added!',
            status_code: 201
        }));
    } catch (e) {
        res.status(400).send(_tools_Response__WEBPACK_IMPORTED_MODULE_4___default()({
            message: e.message,
            status_code: 400
        }));
    }
});


/***/ })

};
;

// load runtime
var __webpack_require__ = require("../../../webpack-api-runtime.js");
__webpack_require__.C(exports);
var __webpack_exec__ = (moduleId) => (__webpack_require__(__webpack_require__.s = moduleId))
var __webpack_exports__ = __webpack_require__.X(0, [6676,8023], () => (__webpack_exec__(7374)));
module.exports = __webpack_exports__;

})();