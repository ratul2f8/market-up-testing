"use strict";
(() => {
var exports = {};
exports.id = 3572;
exports.ids = [3572];
exports.modules = {

/***/ 3518:
/***/ ((module) => {

module.exports = require("cloudinary");

/***/ }),

/***/ 384:
/***/ ((module) => {

module.exports = require("datauri");

/***/ }),

/***/ 2616:
/***/ ((module) => {

module.exports = require("formidable");

/***/ }),

/***/ 1185:
/***/ ((module) => {

module.exports = require("mongoose");

/***/ }),

/***/ 4533:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "config": () => (/* binding */ config),
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _tools_db_connection__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(6704);
/* harmony import */ var _tools_db_connection__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_tools_db_connection__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _tools_db_Model_ClientsModel__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(6775);
/* harmony import */ var _tools_db_Model_ClientsModel__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_tools_db_Model_ClientsModel__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var mongoose__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(1185);
/* harmony import */ var mongoose__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(mongoose__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _tools_FormDataParser__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(2825);
/* harmony import */ var _tools_FormDataParser__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_tools_FormDataParser__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _tools_cloudinary_upload__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(9053);
/* harmony import */ var _tools_cloudinary_upload__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_tools_cloudinary_upload__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _tools_validators_clients_clientDataValidator__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(7900);
/* harmony import */ var _tools_validators_clients_clientDataValidator__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(_tools_validators_clients_clientDataValidator__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _tools_Response__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(3829);
/* harmony import */ var _tools_Response__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(_tools_Response__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var _tools_cloudinary_delete__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(3834);
/* harmony import */ var _tools_cloudinary_delete__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(_tools_cloudinary_delete__WEBPACK_IMPORTED_MODULE_7__);








const config = {
    api: {
        bodyParser: false
    }
};
/**
 * @method PUT
 *
 * @requires FromData
 *
 * @param image File image/png, image/jpg, image/jpeg
 *
 * @return ClientModel
 *
 * */ const handler = async (req, res)=>{
    if (req.method !== 'PUT') {
        return res.status(404).send();
    }
    try {
        const { query: { id: clientId  }  } = req;
        // object id validation
        if (!mongoose__WEBPACK_IMPORTED_MODULE_2___default().Types.ObjectId.isValid(clientId)) {
            return res.status(404).send();
        }
        await _tools_db_connection__WEBPACK_IMPORTED_MODULE_0___default()();
        const client = await _tools_db_Model_ClientsModel__WEBPACK_IMPORTED_MODULE_1___default().findById(clientId);
        // db existence validation
        if (!client) {
            return res.status(404).send();
        }
        // parse the form from FormData
        const { files  } = await _tools_FormDataParser__WEBPACK_IMPORTED_MODULE_3___default()(req);
        const errors = _tools_validators_clients_clientDataValidator__WEBPACK_IMPORTED_MODULE_5___default()({
            files,
            create: false
        });
        // return if error
        if (errors) {
            return res.status(422).send(_tools_Response__WEBPACK_IMPORTED_MODULE_6___default()({
                errors,
                status_code: 422,
                message: 'Invalid input!'
            }));
        }
        const prevId = client.image;
        if (files === null || files === void 0 ? void 0 : files.image) {
            const { public_id  } = await _tools_cloudinary_upload__WEBPACK_IMPORTED_MODULE_4___default()(files.image, 'clients');
            client.image = public_id;
        }
        await client.save();
        // delete previous image if new have
        if (files === null || files === void 0 ? void 0 : files.image) {
            await _tools_cloudinary_delete__WEBPACK_IMPORTED_MODULE_7___default()(prevId);
        }
        return res.status(201).send(_tools_Response__WEBPACK_IMPORTED_MODULE_6___default()({
            data: client,
            status_code: 201,
            message: 'Update complete!'
        }));
    } catch (e) {
        res.status(500).send(_tools_Response__WEBPACK_IMPORTED_MODULE_6___default()({
            message: e.message,
            status_code: 500
        }));
    }
};
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (handler);


/***/ }),

/***/ 3834:
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {


const cloudinary = __webpack_require__(381);
const deleteImage = (publicId)=>cloudinary.v2.uploader.destroy(publicId)
;
module.exports = deleteImage;


/***/ })

};
;

// load runtime
var __webpack_require__ = require("../../../../webpack-api-runtime.js");
__webpack_require__.C(exports);
var __webpack_exec__ = (moduleId) => (__webpack_require__(__webpack_require__.s = moduleId))
var __webpack_exports__ = __webpack_require__.X(0, [6676,8023], () => (__webpack_exec__(4533)));
module.exports = __webpack_exports__;

})();