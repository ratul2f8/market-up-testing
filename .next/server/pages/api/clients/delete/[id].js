"use strict";
(() => {
var exports = {};
exports.id = 8120;
exports.ids = [8120];
exports.modules = {

/***/ 3518:
/***/ ((module) => {

module.exports = require("cloudinary");

/***/ }),

/***/ 1185:
/***/ ((module) => {

module.exports = require("mongoose");

/***/ }),

/***/ 1312:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "config": () => (/* binding */ config),
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _tools_db_connection__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(6704);
/* harmony import */ var _tools_db_connection__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_tools_db_connection__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _tools_db_Model_ClientsModel__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(6775);
/* harmony import */ var _tools_db_Model_ClientsModel__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_tools_db_Model_ClientsModel__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var mongoose__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(1185);
/* harmony import */ var mongoose__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(mongoose__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _tools_Response__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(3829);
/* harmony import */ var _tools_Response__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_tools_Response__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _tools_cloudinary_delete__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(3834);
/* harmony import */ var _tools_cloudinary_delete__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_tools_cloudinary_delete__WEBPACK_IMPORTED_MODULE_4__);





const config = {
    api: {
        bodyParser: false
    }
};
/**
 * @method DELETE
 *
 * @query_params ObjectID
 *
 * @return
 * */ const handler = async (req, res)=>{
    if (req.method !== 'DELETE') {
        return res.status(404).send();
    }
    try {
        const { query: { id: clientId  }  } = req;
        // object id validation
        if (!mongoose__WEBPACK_IMPORTED_MODULE_2___default().Types.ObjectId.isValid(clientId)) {
            return res.status(404).send();
        }
        await _tools_db_connection__WEBPACK_IMPORTED_MODULE_0___default()();
        const client = await _tools_db_Model_ClientsModel__WEBPACK_IMPORTED_MODULE_1___default().findById(clientId);
        // db existence validation
        if (!client) {
            return res.status(404).send();
        }
        const prevId = client.image;
        // delete from db
        await client.delete();
        // delete image form cloudinary
        await _tools_cloudinary_delete__WEBPACK_IMPORTED_MODULE_4___default()(prevId);
        return res.status(201).send(_tools_Response__WEBPACK_IMPORTED_MODULE_3___default()({
            status_code: 200,
            message: 'Client logo deleted successfully!!'
        }));
    } catch (e) {
        res.status(500).send(_tools_Response__WEBPACK_IMPORTED_MODULE_3___default()({
            message: e.message,
            status_code: 500
        }));
    }
};
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (handler);


/***/ }),

/***/ 3829:
/***/ ((module) => {


/**
 *
 * @param data :any
 * @param errors :any
 * @param status_code :int
 * @param message :String
 *
 * @return {data, errors, status_code, message}
 *
 * */ const Response = ({ data , errors , status_code , message  })=>({
        data: data || null,
        errors: errors || null,
        status_code: status_code || 200,
        message: message || ''
    })
;
module.exports = Response;


/***/ }),

/***/ 3834:
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {


const cloudinary = __webpack_require__(381);
const deleteImage = (publicId)=>cloudinary.v2.uploader.destroy(publicId)
;
module.exports = deleteImage;


/***/ }),

/***/ 381:
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {


const cloudinary = __webpack_require__(3518);
cloudinary.config({
    cloud_name: 'dhrjql4ev',
    api_key: '346684836276456',
    api_secret: 'r5GE_y1LszfXliaz3GY6qyDc6Vk'
});
module.exports = cloudinary;


/***/ }),

/***/ 6775:
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {


const mongoose = __webpack_require__(1185);
const clients = new mongoose.Schema({
    image: {
        required: true,
        type: String
    }
}, {
    timestamps: {
        createdAt: 'created_at',
        updatedAt: 'updated_at'
    }
});
const ClientModel = mongoose.models.Clients || mongoose.model('Clients', clients);
module.exports = ClientModel;


/***/ }),

/***/ 6704:
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {


const mongoose = __webpack_require__(1185);
const connectDB = async ()=>{
    if (mongoose.connections[0].readyState) {
        return;
    } else {
        await mongoose.connect(process.env.MONGODB_URL, {
            useUnifiedTopology: true,
            useFindAndModify: false,
            useCreateIndex: true,
            useNewUrlParser: true
        });
        return;
    }
};
module.exports = connectDB;


/***/ })

};
;

// load runtime
var __webpack_require__ = require("../../../../webpack-api-runtime.js");
__webpack_require__.C(exports);
var __webpack_exec__ = (moduleId) => (__webpack_require__(__webpack_require__.s = moduleId))
var __webpack_exports__ = (__webpack_exec__(1312));
module.exports = __webpack_exports__;

})();