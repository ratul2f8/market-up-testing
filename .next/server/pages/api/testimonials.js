"use strict";
(() => {
var exports = {};
exports.id = 8560;
exports.ids = [8560];
exports.modules = {

/***/ 1185:
/***/ ((module) => {

module.exports = require("mongoose");

/***/ }),

/***/ 7101:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _tools_db_Model_TestimonialModel__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(5164);
/* harmony import */ var _tools_db_Model_TestimonialModel__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_tools_db_Model_TestimonialModel__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _tools_db_connection__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(6704);
/* harmony import */ var _tools_db_connection__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_tools_db_connection__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _tools_Response__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(3829);
/* harmony import */ var _tools_Response__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_tools_Response__WEBPACK_IMPORTED_MODULE_2__);



/**
 * @method GET
 *
 * @return [TestimonialModel]
 *
 * */ /* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (async (req, res)=>{
    if (req.method !== 'GET') {
        return res.status(400).send();
    }
    try {
        await _tools_db_connection__WEBPACK_IMPORTED_MODULE_1___default()();
        const testimonials = await _tools_db_Model_TestimonialModel__WEBPACK_IMPORTED_MODULE_0___default().find({
        }).sort({
            'created_at': '-1'
        });
        res.send(_tools_Response__WEBPACK_IMPORTED_MODULE_2___default()({
            status_code: 200,
            data: testimonials
        }));
    } catch (e) {
        res.status(500).send(_tools_Response__WEBPACK_IMPORTED_MODULE_2___default()({
            message: e.message,
            status_code: 500
        }));
    }
});


/***/ }),

/***/ 3829:
/***/ ((module) => {


/**
 *
 * @param data :any
 * @param errors :any
 * @param status_code :int
 * @param message :String
 *
 * @return {data, errors, status_code, message}
 *
 * */ const Response = ({ data , errors , status_code , message  })=>({
        data: data || null,
        errors: errors || null,
        status_code: status_code || 200,
        message: message || ''
    })
;
module.exports = Response;


/***/ }),

/***/ 5164:
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {


const mongoose = __webpack_require__(1185);
const testimonials = new mongoose.Schema({
    text: {
        required: true,
        type: String
    },
    name: {
        required: true,
        type: String,
        trim: true
    },
    designation: {
        required: true,
        type: String,
        trim: true
    },
    company: {
        required: true,
        type: String,
        trim: true
    }
}, {
    timestamps: {
        createdAt: 'created_at',
        updatedAt: 'updated_at'
    }
});
const TestimonialModel = mongoose.models.Testimonials || mongoose.model('Testimonials', testimonials);
module.exports = TestimonialModel;


/***/ }),

/***/ 6704:
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {


const mongoose = __webpack_require__(1185);
const connectDB = async ()=>{
    if (mongoose.connections[0].readyState) {
        return;
    } else {
        await mongoose.connect(process.env.MONGODB_URL, {
            useUnifiedTopology: true,
            useFindAndModify: false,
            useCreateIndex: true,
            useNewUrlParser: true
        });
        return;
    }
};
module.exports = connectDB;


/***/ })

};
;

// load runtime
var __webpack_require__ = require("../../webpack-api-runtime.js");
__webpack_require__.C(exports);
var __webpack_exec__ = (moduleId) => (__webpack_require__(__webpack_require__.s = moduleId))
var __webpack_exports__ = (__webpack_exec__(7101));
module.exports = __webpack_exports__;

})();