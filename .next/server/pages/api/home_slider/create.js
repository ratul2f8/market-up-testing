"use strict";
(() => {
var exports = {};
exports.id = 2979;
exports.ids = [2979];
exports.modules = {

/***/ 3518:
/***/ ((module) => {

module.exports = require("cloudinary");

/***/ }),

/***/ 384:
/***/ ((module) => {

module.exports = require("datauri");

/***/ }),

/***/ 2616:
/***/ ((module) => {

module.exports = require("formidable");

/***/ }),

/***/ 1185:
/***/ ((module) => {

module.exports = require("mongoose");

/***/ }),

/***/ 1894:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "config": () => (/* binding */ config),
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _tools_FormDataParser__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(2825);
/* harmony import */ var _tools_FormDataParser__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_tools_FormDataParser__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _tools_cloudinary_upload__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(9053);
/* harmony import */ var _tools_cloudinary_upload__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_tools_cloudinary_upload__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _tools_db_Model_HomeSlider__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(785);
/* harmony import */ var _tools_db_Model_HomeSlider__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_tools_db_Model_HomeSlider__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _tools_db_connection__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(6704);
/* harmony import */ var _tools_db_connection__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_tools_db_connection__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _tools_Response__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(3829);
/* harmony import */ var _tools_Response__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_tools_Response__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _tools_validators_homeSlider_createHomeSliderValidator__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(4446);
/* harmony import */ var _tools_validators_homeSlider_createHomeSliderValidator__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(_tools_validators_homeSlider_createHomeSliderValidator__WEBPACK_IMPORTED_MODULE_5__);





// import validators

const config = {
    api: {
        bodyParser: false
    }
};
/**
 * @method POST
 *
 * @requires FromData

 * @param image File image/png, image/jpg, image/jpeg, image/gif
 *
 * @return HomeSliderModel
 *
 * */ /* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (async (req, res)=>{
    const { method  } = req;
    if (method != 'POST') {
        return res.status(404).send();
    }
    try {
        const { files  } = await _tools_FormDataParser__WEBPACK_IMPORTED_MODULE_0___default()(req);
        // const create = true;
        const errors = _tools_validators_homeSlider_createHomeSliderValidator__WEBPACK_IMPORTED_MODULE_5___default()({
            files,
            create: true
        });
        if (errors) {
            return res.status(422).send(_tools_Response__WEBPACK_IMPORTED_MODULE_4___default()({
                status_code: 422,
                errors,
                message: 'Invalid input!'
            }));
        }
        await _tools_db_connection__WEBPACK_IMPORTED_MODULE_3___default()();
        const { public_id  } = await _tools_cloudinary_upload__WEBPACK_IMPORTED_MODULE_1___default()(files.image, 'home_slider');
        // save data to db
        const slide = await _tools_db_Model_HomeSlider__WEBPACK_IMPORTED_MODULE_2___default().create({
            image: public_id
        });
        // return to client
        return res.status(201).send(_tools_Response__WEBPACK_IMPORTED_MODULE_4___default()({
            data: slide,
            message: 'New slide added!',
            status_code: 201
        }));
    } catch (e) {
        res.status(400).send(_tools_Response__WEBPACK_IMPORTED_MODULE_4___default()({
            message: e.message,
            status_code: 400
        }));
    }
});


/***/ }),

/***/ 381:
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {


const cloudinary = __webpack_require__(3518);
cloudinary.config({
    cloud_name: 'dhrjql4ev',
    api_key: '346684836276456',
    api_secret: 'r5GE_y1LszfXliaz3GY6qyDc6Vk'
});
module.exports = cloudinary;


/***/ }),

/***/ 9053:
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {


const cloudinary = __webpack_require__(381);
const datauri = __webpack_require__(384);
/**
 *
 * @param file File
 * @param folder String
 *
 * @return {public_id, ...}
 *
 * */ module.exports = (file, folder)=>{
    return new Promise((resolve, reject)=>{
        datauri(file.path).then((bs)=>{
            cloudinary.v2.uploader.upload(bs, {
                folder: folder
            }, (err, result)=>{
                if (err) {
                    reject(err);
                }
                resolve(result);
            });
        }).catch((err)=>{
            reject(err);
        });
    });
};


/***/ }),

/***/ 785:
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {


var ref;
const mongoose = __webpack_require__(1185);
const homeSliderSchema = new mongoose.Schema({
    image: String
}, {
    timestamps: {
        createdAt: 'created_at',
        updatedAt: 'updated_at'
    }
});
const HomeSliderModel = ((ref = mongoose.models) === null || ref === void 0 ? void 0 : ref.HomeSlider) || mongoose.model('HomeSlider', homeSliderSchema);
module.exports = HomeSliderModel;


/***/ }),

/***/ 4446:
/***/ ((module) => {


const createHomeSliderValidator = ({ files , create  })=>{
    const errors = {
    };
    // image validator
    if (create) {
        if (files === null || files === void 0 ? void 0 : files.image) {
            if (files.image.type == 'image/png' || files.image.type == 'image/jpg' || files.image.type == 'image/jpeg' || files.image.type == 'image/gif') {
            } else {
                errors.image = 'Only \'jpg, png, jpeg, gif\' are allowed!';
            }
        } else {
            errors.image = 'Image is required!';
        }
    } else {
        if (files === null || files === void 0 ? void 0 : files.image) {
            if (files.image.type == 'image/png' || files.image.type == 'image/jpg' || files.image.type == 'image/jpeg' || files.image.type == 'image/gif') {
            } else {
                errors.image = 'Only \'jpg, png, jpeg, gif\' are allowed!';
            }
        }
    }
    if (Object.keys(errors).length) {
        return errors;
    }
    return null;
};
module.exports = createHomeSliderValidator;


/***/ })

};
;

// load runtime
var __webpack_require__ = require("../../../webpack-api-runtime.js");
__webpack_require__.C(exports);
var __webpack_exec__ = (moduleId) => (__webpack_require__(__webpack_require__.s = moduleId))
var __webpack_exports__ = __webpack_require__.X(0, [6676], () => (__webpack_exec__(1894)));
module.exports = __webpack_exports__;

})();