exports.id = 681;
exports.ids = [681];
exports.modules = {

/***/ 8322:
/***/ ((module) => {

// Exports
module.exports = {
	"desktop-nav": "Header_desktop-nav__37SSL",
	"mobile-nav": "Header_mobile-nav__dBjZt"
};


/***/ }),

/***/ 681:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";

// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "Z": () => (/* binding */ layouts_HomeLayout)
});

// EXTERNAL MODULE: external "react/jsx-runtime"
var jsx_runtime_ = __webpack_require__(997);
// EXTERNAL MODULE: external "react"
var external_react_ = __webpack_require__(6689);
// EXTERNAL MODULE: external "react-icons/fa"
var fa_ = __webpack_require__(6290);
// EXTERNAL MODULE: ./node_modules/next/link.js
var next_link = __webpack_require__(1664);
;// CONCATENATED MODULE: ./layouts/HomeLayout/Header/menuItems.js


const SubNavItems = ()=>/*#__PURE__*/ (0,jsx_runtime_.jsxs)(jsx_runtime_.Fragment, {
        children: [
            /*#__PURE__*/ jsx_runtime_.jsx("li", {
                children: /*#__PURE__*/ jsx_runtime_.jsx(next_link["default"], {
                    href: '/',
                    children: "Home"
                })
            }),
            /*#__PURE__*/ jsx_runtime_.jsx("li", {
                children: /*#__PURE__*/ jsx_runtime_.jsx(next_link["default"], {
                    href: '/#about',
                    children: "About"
                })
            }),
            /*#__PURE__*/ jsx_runtime_.jsx("li", {
                children: /*#__PURE__*/ jsx_runtime_.jsx("a", {
                    href: '/#service',
                    children: "Service"
                })
            }),
            /*#__PURE__*/ jsx_runtime_.jsx("li", {
                children: /*#__PURE__*/ jsx_runtime_.jsx(next_link["default"], {
                    href: '/#team',
                    children: "Our Team"
                })
            }),
            /*#__PURE__*/ jsx_runtime_.jsx("li", {
                children: /*#__PURE__*/ jsx_runtime_.jsx(next_link["default"], {
                    href: '/#work',
                    children: "Work"
                })
            }),
            /*#__PURE__*/ jsx_runtime_.jsx("li", {
                children: /*#__PURE__*/ jsx_runtime_.jsx(next_link["default"], {
                    href: '/#client',
                    children: "Client"
                })
            }),
            /*#__PURE__*/ jsx_runtime_.jsx("li", {
                children: /*#__PURE__*/ jsx_runtime_.jsx(next_link["default"], {
                    href: '/#contact',
                    children: "Contact"
                })
            }),
            /*#__PURE__*/ jsx_runtime_.jsx("li", {
                children: /*#__PURE__*/ jsx_runtime_.jsx(next_link["default"], {
                    href: '/career',
                    children: "Career"
                })
            })
        ]
    })
;
/* harmony default export */ const menuItems = (SubNavItems);

// EXTERNAL MODULE: ./layouts/HomeLayout/Header/Header.module.css
var Header_module = __webpack_require__(8322);
var Header_module_default = /*#__PURE__*/__webpack_require__.n(Header_module);
;// CONCATENATED MODULE: ./layouts/HomeLayout/Header/index.js






const Header = ()=>{
    const { 0: open , 1: setOpen  } = (0,external_react_.useState)(false);
    return(/*#__PURE__*/ jsx_runtime_.jsx("header", {
        className: "shadow bg-white z-50 border-b-2 border-gray-300 sticky -top-0 -left-0",
        children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
            className: "grid grid-cols-4",
            children: [
                /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                    className: "pl-2 md:pl-5 flex items-center cursor-pointer",
                    children: [
                        /*#__PURE__*/ jsx_runtime_.jsx(next_link["default"], {
                            href: "/",
                            children: /*#__PURE__*/ jsx_runtime_.jsx("img", {
                                src: "/Images/logo.gif",
                                className: "w-16",
                                alt: "Logo"
                            })
                        }),
                        /*#__PURE__*/ jsx_runtime_.jsx(next_link["default"], {
                            href: "/",
                            children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("p", {
                                className: "text-secondary font-typoSlab text-xl md:text-2xl z-10",
                                children: [
                                    "market",
                                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("span", {
                                        className: "text-primary",
                                        children: [
                                            "UP",
                                            /*#__PURE__*/ jsx_runtime_.jsx("span", {
                                                className: 'font-poppins',
                                                children: "."
                                            })
                                        ]
                                    })
                                ]
                            })
                        })
                    ]
                }),
                /*#__PURE__*/ jsx_runtime_.jsx("div", {
                    className: "col-span-3 flex items-center justify-end font-poppins",
                    children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("nav", {
                        children: [
                            /*#__PURE__*/ jsx_runtime_.jsx("ul", {
                                className: (Header_module_default())["desktop-nav"],
                                children: /*#__PURE__*/ jsx_runtime_.jsx(menuItems, {
                                })
                            }),
                            /*#__PURE__*/ jsx_runtime_.jsx(fa_.FaAlignRight, {
                                onClick: ()=>setOpen(!open)
                                ,
                                className: "text-gray-600 inline-block lg:hidden mr-5 cursor-pointer",
                                size: 24
                            }),
                            /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                className: `fixed z-30 w-2/3 transition-all -top-0 duration-500 bg-white h-screen ${open ? "-left-0" : "-left-full"}`,
                                children: [
                                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                        className: "pl-4 flex items-center cursor-pointer",
                                        children: [
                                            /*#__PURE__*/ jsx_runtime_.jsx(next_link["default"], {
                                                href: "/",
                                                children: /*#__PURE__*/ jsx_runtime_.jsx("img", {
                                                    src: "Images/logo.gif",
                                                    className: "w-16",
                                                    alt: "logo"
                                                })
                                            }),
                                            /*#__PURE__*/ jsx_runtime_.jsx(next_link["default"], {
                                                href: "/",
                                                children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("p", {
                                                    className: "text-secondary font-typoSlab text-xl md:text-2xl z-10",
                                                    children: [
                                                        "market",
                                                        /*#__PURE__*/ (0,jsx_runtime_.jsxs)("span", {
                                                            className: "text-primary",
                                                            children: [
                                                                "UP",
                                                                /*#__PURE__*/ jsx_runtime_.jsx("span", {
                                                                    className: "font-poppins",
                                                                    children: "."
                                                                })
                                                            ]
                                                        })
                                                    ]
                                                })
                                            })
                                        ]
                                    }),
                                    /*#__PURE__*/ jsx_runtime_.jsx("hr", {
                                    }),
                                    /*#__PURE__*/ jsx_runtime_.jsx("ul", {
                                        className: (Header_module_default())["mobile-nav"],
                                        onClick: ()=>setOpen(false)
                                        ,
                                        children: /*#__PURE__*/ jsx_runtime_.jsx(menuItems, {
                                        })
                                    })
                                ]
                            }),
                            /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                onClick: ()=>setOpen(false)
                                ,
                                className: `z-10 fixed w-full transition-all -top-0 duration-150 bg-black bg-opacity-20 h-screen ${open ? "-right-0 opacity-100" : "right-full opacity-0"}`
                            })
                        ]
                    })
                })
            ]
        })
    }));
};
/* harmony default export */ const HomeLayout_Header = (Header);

;// CONCATENATED MODULE: ./layouts/HomeLayout/Footer/index.js



const Footer = ()=>{
    return(/*#__PURE__*/ jsx_runtime_.jsx(jsx_runtime_.Fragment, {
        children: /*#__PURE__*/ jsx_runtime_.jsx("div", {
            className: 'bg-dark',
            children: /*#__PURE__*/ jsx_runtime_.jsx("div", {
                className: "container mx-auto px-5 md:px-24 ",
                children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                    className: "grid md:grid-cols-3",
                    children: [
                        /*#__PURE__*/ jsx_runtime_.jsx("div", {
                            className: 'md:flex items-center py-4 md:py-10 hidden ',
                            children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                children: [
                                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("p", {
                                        className: 'text-xs font-medium font-poppins text-white',
                                        children: [
                                            /*#__PURE__*/ jsx_runtime_.jsx("span", {
                                                className: 'pr-1 text-base',
                                                children: "\xa9"
                                            }),
                                            " 2021 marketUP. Group Communications"
                                        ]
                                    }),
                                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("p", {
                                        className: 'text-xs font-poppins py-1 text-white',
                                        children: [
                                            /*#__PURE__*/ jsx_runtime_.jsx("span", {
                                                className: 'pr-1 text-base opacity-0',
                                                children: "\xa9"
                                            }),
                                            " All Rights Reserved"
                                        ]
                                    })
                                ]
                            })
                        }),
                        /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                            className: 'md:py-10 pl-2 md:pl-10 py-2',
                            children: [
                                /*#__PURE__*/ jsx_runtime_.jsx("h2", {
                                    className: 'text-sm font-poppins text-white font-medium py-1 md:py-3',
                                    children: "About"
                                }),
                                /*#__PURE__*/ jsx_runtime_.jsx("p", {
                                    className: "py-1 md:py-2 font-normal text-xs font poppins text-white",
                                    children: /*#__PURE__*/ jsx_runtime_.jsx(next_link["default"], {
                                        href: '/career',
                                        children: "Career"
                                    })
                                }),
                                /*#__PURE__*/ jsx_runtime_.jsx("p", {
                                    className: "py-1 md:py-2 font-normal text-xs font poppins text-white",
                                    children: /*#__PURE__*/ jsx_runtime_.jsx(next_link["default"], {
                                        href: '/#client',
                                        children: "Client"
                                    })
                                }),
                                /*#__PURE__*/ jsx_runtime_.jsx("p", {
                                    className: "py-1 md:py-2 font-normal text-xs font poppins text-white",
                                    children: /*#__PURE__*/ jsx_runtime_.jsx(next_link["default"], {
                                        href: '/#about',
                                        children: "About Us"
                                    })
                                })
                            ]
                        }),
                        /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                            className: 'md:py-10 pl-2 md:pl-10 py-2',
                            children: [
                                /*#__PURE__*/ jsx_runtime_.jsx("h2", {
                                    className: 'text-sm font-poppins text-white font-medium py-1 md:py-3',
                                    children: "Social Media"
                                }),
                                /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                    className: 'text-primary pt-3 md:pt-7',
                                    children: [
                                        /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                            className: 'mr-2 inline-flex h-8 w-8 rounded-full bg-white justify-center items-center cursor-pointer',
                                            children: /*#__PURE__*/ jsx_runtime_.jsx("a", {
                                                target: "_blank",
                                                href: 'https://www.facebook.com/marketupdigital/',
                                                rel: "noopener noreferrer",
                                                children: /*#__PURE__*/ jsx_runtime_.jsx(fa_.FaFacebookF, {
                                                })
                                            })
                                        }),
                                        /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                            className: 'mr-2 inline-flex h-8 w-8 rounded-full bg-white justify-center items-center',
                                            children: /*#__PURE__*/ jsx_runtime_.jsx("a", {
                                                target: "_blank",
                                                href: 'https://instagram.com/marketupgroup?utm_medium=copy_link',
                                                rel: "noopener noreferrer",
                                                children: /*#__PURE__*/ jsx_runtime_.jsx(fa_.FaInstagram, {
                                                })
                                            })
                                        }),
                                        /*#__PURE__*/ jsx_runtime_.jsx("div", {
                                            className: 'inline-flex h-8 w-8 rounded-full bg-white justify-center items-center',
                                            children: /*#__PURE__*/ jsx_runtime_.jsx("a", {
                                                target: "_blank",
                                                href: 'https://www.linkedin.com/company/marketupgroup',
                                                rel: "noopener noreferrer",
                                                children: /*#__PURE__*/ jsx_runtime_.jsx(fa_.FaLinkedinIn, {
                                                })
                                            })
                                        })
                                    ]
                                })
                            ]
                        }),
                        /*#__PURE__*/ jsx_runtime_.jsx("div", {
                            className: 'flex items-center py-4 md:py-10 md:hidden ',
                            children: /*#__PURE__*/ (0,jsx_runtime_.jsxs)("div", {
                                children: [
                                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("p", {
                                        className: 'text-xs font-medium font-poppins text-white',
                                        children: [
                                            /*#__PURE__*/ jsx_runtime_.jsx("span", {
                                                className: 'pr-1 text-base',
                                                children: "\xa9"
                                            }),
                                            " 2021 marketUP. Group Communications"
                                        ]
                                    }),
                                    /*#__PURE__*/ (0,jsx_runtime_.jsxs)("p", {
                                        className: 'text-xs font-poppins py-1 text-white',
                                        children: [
                                            /*#__PURE__*/ jsx_runtime_.jsx("span", {
                                                className: 'pr-1 text-base opacity-0',
                                                children: "\xa9"
                                            }),
                                            " All Rights Reserved"
                                        ]
                                    })
                                ]
                            })
                        })
                    ]
                })
            })
        })
    }));
};
/* harmony default export */ const HomeLayout_Footer = (Footer);

;// CONCATENATED MODULE: ./layouts/HomeLayout/index.js



function HomeLayout({ children  }) {
    return(/*#__PURE__*/ (0,jsx_runtime_.jsxs)(jsx_runtime_.Fragment, {
        children: [
            /*#__PURE__*/ jsx_runtime_.jsx("link", {
                rel: "preload",
                href: "public/fonts/Typoslab_ irregular/TypoSlab Irregular Demo.otf",
                as: "font",
                crossOrigin: ""
            }),
            /*#__PURE__*/ jsx_runtime_.jsx(HomeLayout_Header, {
            }),
            children,
            /*#__PURE__*/ jsx_runtime_.jsx(HomeLayout_Footer, {
            })
        ]
    }));
}
;
/* harmony default export */ const layouts_HomeLayout = (HomeLayout);


/***/ })

};
;