"use strict";
exports.id = 5671;
exports.ids = [5671];
exports.modules = {

/***/ 5671:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(997);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var cloudinary_react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(395);
/* harmony import */ var cloudinary_react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(cloudinary_react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _frontend_env__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(6646);



const TeamCard = ({ image , name , designation  })=>{
    return(/*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
        children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
            className: 'border border-gray-300 rounded-md mt-5 md:mt-10',
            children: [
                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                    className: 'grid justify-center mb-5 mt-10',
                    children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(cloudinary_react__WEBPACK_IMPORTED_MODULE_1__.Image, {
                        className: 'h-40 w-40 rounded-full object-cover',
                        cloudName: _frontend_env__WEBPACK_IMPORTED_MODULE_2__/* .cloudinaryCloudName */ .l,
                        publicId: image,
                        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(cloudinary_react__WEBPACK_IMPORTED_MODULE_1__.Transformation, {
                            width: 300,
                            height: 300,
                            gravity: "south",
                            crop: "fill"
                        })
                    })
                }),
                /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                    className: 'text-center mt-5 mb-10',
                    children: [
                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("p", {
                            className: 'font-poppins text-lg lg:text-2xl font-medium text-gray-200',
                            children: name
                        }),
                        /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("p", {
                            className: 'font-poppins text-sm lg:text-lg text-gray-200 mt-3',
                            children: designation
                        })
                    ]
                })
            ]
        })
    }));
};
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (TeamCard);


/***/ })

};
;