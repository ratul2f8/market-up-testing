"use strict";
exports.id = 5592;
exports.ids = [5592];
exports.modules = {

/***/ 2655:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(997);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var cloudinary_react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(395);
/* harmony import */ var cloudinary_react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(cloudinary_react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _frontend_env__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(6646);



const PartnershipCard = ({ name , designation , image , companyLogo , text  })=>{
    return(/*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
        className: 'container mx-auto',
        children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
            className: 'grid grid-cols-12',
            children: [
                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                    className: 'hidden md:block col-span-2',
                    children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                        className: 'grid place-items-center h-full',
                        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(cloudinary_react__WEBPACK_IMPORTED_MODULE_1__.Image, {
                            className: 'w-52 h-16',
                            cloudName: _frontend_env__WEBPACK_IMPORTED_MODULE_2__/* .cloudinaryCloudName */ .l,
                            publicId: companyLogo,
                            children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(cloudinary_react__WEBPACK_IMPORTED_MODULE_1__.Transformation, {
                                width: "400",
                                gravity: "south",
                                crop: "fill"
                            })
                        })
                    })
                }),
                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                    className: 'col-span-12 md:col-span-4 grid place-items-center',
                    children: /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                        className: 'rounded-md',
                        children: [
                            /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                                className: 'grid justify-center',
                                children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(cloudinary_react__WEBPACK_IMPORTED_MODULE_1__.Image, {
                                    className: 'h-40 w-40 rounded-full object-cover',
                                    cloudName: _frontend_env__WEBPACK_IMPORTED_MODULE_2__/* .cloudinaryCloudName */ .l,
                                    publicId: image,
                                    children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(cloudinary_react__WEBPACK_IMPORTED_MODULE_1__.Transformation, {
                                        width: "400",
                                        gravity: "south",
                                        crop: "fill"
                                    })
                                })
                            }),
                            /*#__PURE__*/ (0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                                className: 'text-center mt-5 mb-3 md:mb-10 text-gray-200',
                                children: [
                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("p", {
                                        className: 'font-poppins text-base lg:text-2xl font-medium',
                                        children: name
                                    }),
                                    /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("p", {
                                        className: 'font-poppins text-sm lg:text-xl mt-3',
                                        children: designation
                                    })
                                ]
                            })
                        ]
                    })
                }),
                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                    className: 'md:hidden col-span-12',
                    children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                        className: 'grid place-items-center mb-3',
                        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(cloudinary_react__WEBPACK_IMPORTED_MODULE_1__.Image, {
                            className: 'md:h-16 h-10',
                            cloudName: _frontend_env__WEBPACK_IMPORTED_MODULE_2__/* .cloudinaryCloudName */ .l,
                            publicId: companyLogo,
                            children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(cloudinary_react__WEBPACK_IMPORTED_MODULE_1__.Transformation, {
                                width: "400",
                                gravity: "south",
                                crop: "fill"
                            })
                        })
                    })
                }),
                /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                    className: ' col-span-12 md:col-span-6',
                    children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                        className: 'font-poppins md:text-sm text-xs text-center text-gray-300 md:mt-5',
                        children: /*#__PURE__*/ react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("p", {
                            children: text
                        })
                    })
                })
            ]
        })
    }));
};
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (PartnershipCard);


/***/ }),

/***/ 6646:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "l": () => (/* binding */ cloudinaryCloudName)
/* harmony export */ });
const cloudinaryCloudName = 'dhrjql4ev';


/***/ }),

/***/ 1979:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(2167);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_0__);

const axiosPost = (url, body, headers)=>{
    return new Promise((resolve, reject)=>{
        axios__WEBPACK_IMPORTED_MODULE_0___default().post(url, body, {
            headers: headers
        }).then((res)=>resolve(res.data)
        ).catch((err)=>reject(err.response.data)
        );
    });
};
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (axiosPost);


/***/ })

};
;