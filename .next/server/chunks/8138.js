"use strict";
exports.id = 8138;
exports.ids = [8138];
exports.modules = {

/***/ 5164:
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {


const mongoose = __webpack_require__(1185);
const testimonials = new mongoose.Schema({
    text: {
        required: true,
        type: String
    },
    name: {
        required: true,
        type: String,
        trim: true
    },
    designation: {
        required: true,
        type: String,
        trim: true
    },
    company: {
        required: true,
        type: String,
        trim: true
    }
}, {
    timestamps: {
        createdAt: 'created_at',
        updatedAt: 'updated_at'
    }
});
const TestimonialModel = mongoose.models.Testimonials || mongoose.model('Testimonials', testimonials);
module.exports = TestimonialModel;


/***/ }),

/***/ 2374:
/***/ ((module) => {


const testimonialDataValidator = ({ fields  })=>{
    const errors = {
    };
    //name validator
    if (fields === null || fields === void 0 ? void 0 : fields.name) {
        if (fields.name.trim().length < 3) {
            errors.name = 'Minimum 3 character allows!';
        }
    } else {
        errors.name = 'Name is required!';
    }
    //text validator
    if ((fields === null || fields === void 0 ? void 0 : fields.text) == null || (fields === null || fields === void 0 ? void 0 : fields.text) === '') {
        errors.text = 'Text is required!';
    }
    //designation validator
    if ((fields === null || fields === void 0 ? void 0 : fields.designation) == null || (fields === null || fields === void 0 ? void 0 : fields.designation) === '') {
        errors.designation = 'Designation is required!';
    }
    //company validator
    if ((fields === null || fields === void 0 ? void 0 : fields.company) == null || (fields === null || fields === void 0 ? void 0 : fields.company) === '') {
        errors.company = 'Company is required!';
    }
    if (Object.keys(errors).length) {
        return errors;
    }
    return null;
};
module.exports = testimonialDataValidator;


/***/ })

};
;