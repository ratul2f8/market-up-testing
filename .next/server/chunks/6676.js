"use strict";
exports.id = 6676;
exports.ids = [6676];
exports.modules = {

/***/ 2825:
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {


const Formidable = __webpack_require__(2616);
/**
 *
 * @param req :Request
 *
 * @return {files, fields}
 *
 * */ module.exports = (req)=>new Promise((resolve, reject)=>{
        const form = new Formidable({
            multiples: true
        });
        form.parse(req, (err, fields, files)=>{
            if (err) {
                reject(err);
            }
            resolve({
                files,
                fields
            });
        });
    })
;


/***/ }),

/***/ 3829:
/***/ ((module) => {


/**
 *
 * @param data :any
 * @param errors :any
 * @param status_code :int
 * @param message :String
 *
 * @return {data, errors, status_code, message}
 *
 * */ const Response = ({ data , errors , status_code , message  })=>({
        data: data || null,
        errors: errors || null,
        status_code: status_code || 200,
        message: message || ''
    })
;
module.exports = Response;


/***/ }),

/***/ 6704:
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {


const mongoose = __webpack_require__(1185);
const connectDB = async ()=>{
    if (mongoose.connections[0].readyState) {
        return;
    } else {
        await mongoose.connect(process.env.MONGODB_URL, {
            useUnifiedTopology: true,
            useFindAndModify: false,
            useCreateIndex: true,
            useNewUrlParser: true
        });
        return;
    }
};
module.exports = connectDB;


/***/ })

};
;