"use strict";
exports.id = 6177;
exports.ids = [6177];
exports.modules = {

/***/ 5388:
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {


const mongoose = __webpack_require__(1185);
const jobs = new mongoose.Schema({
    position: {
        required: true,
        type: String,
        trim: true
    },
    vacancy: {
        required: true,
        type: Number
    },
    jobType: {
        required: true,
        type: String,
        trim: true
    },
    lastDate: {
        required: true,
        type: Date
    },
    fb: {
        type: String,
        trim: true
    },
    insta: {
        type: String,
        trim: true
    }
}, {
    timestamps: {
        createdAt: 'created_at',
        updatedAt: 'updated_at'
    }
});
const JobsModel = mongoose.models.Jobs || mongoose.model('Jobs', jobs);
module.exports = JobsModel;


/***/ }),

/***/ 5770:
/***/ ((module) => {


const jobsDataValidator = ({ fields  })=>{
    const errors = {
    };
    //Position validator
    if ((fields === null || fields === void 0 ? void 0 : fields.position) == null || (fields === null || fields === void 0 ? void 0 : fields.position) === '') {
        errors.position = 'Position is required!';
    }
    //Vacancy validator
    if ((fields === null || fields === void 0 ? void 0 : fields.vacancy) == null || (fields === null || fields === void 0 ? void 0 : fields.vacancy) === '') {
        errors.vacancy = 'Vacancy is required!';
    }
    //Job Type validator
    if ((fields === null || fields === void 0 ? void 0 : fields.jobType) == null || (fields === null || fields === void 0 ? void 0 : fields.jobType) === '') {
        errors.jobType = 'Job Type is required!';
    }
    //Last Date validator
    if ((fields === null || fields === void 0 ? void 0 : fields.lastDate) == null || (fields === null || fields === void 0 ? void 0 : fields.lastDate) === '') {
        errors.lastDate = 'Last Date is required!';
    }
    if (Object.keys(errors).length) {
        return errors;
    }
    return null;
};
module.exports = jobsDataValidator;


/***/ })

};
;