"use strict";
exports.id = 8023;
exports.ids = [8023];
exports.modules = {

/***/ 381:
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {


const cloudinary = __webpack_require__(3518);
cloudinary.config({
    cloud_name: 'dhrjql4ev',
    api_key: '346684836276456',
    api_secret: 'r5GE_y1LszfXliaz3GY6qyDc6Vk'
});
module.exports = cloudinary;


/***/ }),

/***/ 9053:
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {


const cloudinary = __webpack_require__(381);
const datauri = __webpack_require__(384);
/**
 *
 * @param file File
 * @param folder String
 *
 * @return {public_id, ...}
 *
 * */ module.exports = (file, folder)=>{
    return new Promise((resolve, reject)=>{
        datauri(file.path).then((bs)=>{
            cloudinary.v2.uploader.upload(bs, {
                folder: folder
            }, (err, result)=>{
                if (err) {
                    reject(err);
                }
                resolve(result);
            });
        }).catch((err)=>{
            reject(err);
        });
    });
};


/***/ }),

/***/ 6775:
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {


const mongoose = __webpack_require__(1185);
const clients = new mongoose.Schema({
    image: {
        required: true,
        type: String
    }
}, {
    timestamps: {
        createdAt: 'created_at',
        updatedAt: 'updated_at'
    }
});
const ClientModel = mongoose.models.Clients || mongoose.model('Clients', clients);
module.exports = ClientModel;


/***/ }),

/***/ 7900:
/***/ ((module) => {


const clientDataValidator = ({ files , create  })=>{
    const errors = {
    };
    // image validator
    if (create) {
        if (files === null || files === void 0 ? void 0 : files.image) {
            if (files.image.type == 'image/png' || files.image.type == 'image/jpg' || files.image.type == 'image/jpeg') {
            } else {
                errors.image = 'Only \'jpg, png, jpeg\' are allowed!';
            }
        } else {
            errors.image = 'Image is required!';
        }
    } else {
        if (files === null || files === void 0 ? void 0 : files.image) {
            if (files.image.type == 'image/png' || files.image.type == 'image/jpg' || files.image.type == 'image/jpeg') {
            } else {
                errors.image = 'Only \'jpg, png, jpeg\' are allowed!';
            }
        }
    }
    if (Object.keys(errors).length) {
        return errors;
    }
    return null;
};
module.exports = clientDataValidator;


/***/ })

};
;