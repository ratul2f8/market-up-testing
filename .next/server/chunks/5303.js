"use strict";
exports.id = 5303;
exports.ids = [5303];
exports.modules = {

/***/ 5303:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(2167);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_0__);

const axiosGet = (url, headers)=>{
    return new Promise((resolve, reject)=>{
        axios__WEBPACK_IMPORTED_MODULE_0___default().get(url, {
            headers: headers
        }).then((res)=>resolve(res.data)
        ).catch((err)=>reject(err.response.data)
        );
    });
};
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (axiosGet);


/***/ })

};
;